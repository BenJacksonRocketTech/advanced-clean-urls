# INSTALLATION

Install the module from the Backoffice (administration panel), download the release ZIP file (***zzcleanurls.zip***) as it already contains the right folder name (`zzcleanurls`, **not** `zzcleanurls-version_x.y.z` !)

In the modules tab, click on **add a new module**. Click on Browse to open the dialogue box letting you search your computer, select the ZIP file then validate the dialogue box. Finally click on Upload this module.

## Make sure your SEO and URL settings are as follows:

This is __MANDATORY__
 * products:         {category:/}{rewrite}              (you **can** add .html at the end)
 * categories:       {categories:/}{rewrite}**/**
 * manufacturers:    manufactures/{rewrite}
 * suppliers:        suppliers/{rewrite}
 * CMS page:         info/{rewrite}                       (you **can** add .html at the end)
 * CMS category:     info/{rewrite}**/**
 * modules:          modules/{module}{/:controller}

You can replace words such as "info", "suppliers", etc with whatever you want, given that it does not conflicts with a category name

Remember to
 * **clear the browser cache**
 * **clear PS cache** (under smarty -> cache and smarty -> compile)

# UNINSTALLATION

* Go to modules -> Find and uninstall "**zzcleanurls**"

**It should suffice!**


If something goes wrong do the following:
* Open folder /override/classes/
 * Remove "Link.php"
 * Remove "Dispatcher.php"
* Open folder /override/controllers/front/
 * Remove "CategoryController.php"
 * Remove "CmsController.php"
 * Remove "ManufacturerController.php"
 * Remove "ProductController.php"
 * Remove "SupplierController.php"
* Open folder /cache/
 * Remove "class_index.php"
* Go to back office -> Preferences -> SEO and URLs -> Set userfriendly URL off -> Save
* Go to back office -> Preferences -> SEO and URLs -> Set userfriendly URL on -> Save


If you got any other override modules, you should now go to you back office, uninstall them, and reinstall them again to work correctly.

# License

![Creative Commons BY-NC-SA License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)


**[ZiZuu CleanURLs](https://github.com/ZiZuu-store/zzCleanURLs)** by [ZiZuu Store](https://github.com/ZiZuu-store) is licensed under a **Creative Commons [Attribution-NonCommercial-ShareAlike](http://creativecommons.org/licenses/by-nc-sa/4.0/) 4.0 International License**.

Permissions beyond the scope of this license may be available contacting us at info@ZiZuu.com.