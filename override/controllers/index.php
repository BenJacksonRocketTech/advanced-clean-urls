<?php
/**
 * This file is part of the advancedCleanURLs module.
 *
 * @author    RocketTech www.rockettech.co.uk
 * @copyright RocketTech
 * @license   RocketTech
 */

header('Expires: Fri, 31 Dec 1999 23:59:59 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');

header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

header('Location: ../');
return;
